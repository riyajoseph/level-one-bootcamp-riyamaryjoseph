//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>

int input_1()
{
        int x;
        printf("Enter the first number:\n");
        scanf("%d",&x);
        return x;
}

int input_2()
{
        int y;
        printf("Enter the second number:\n");
        scanf("%d",&y);
        return y;
}

int sum(int x, int y)
{
        int sum;
        sum = x + y;
        return sum;
}

int output(int sum)
{ 
        printf("The sum of the numbers is %d\n",sum);
}

int main()
{
        int a,b,c;
        a=input_1();
        b=input_2();
        c=sum(a,b);
        output(c);
        return 0;
}