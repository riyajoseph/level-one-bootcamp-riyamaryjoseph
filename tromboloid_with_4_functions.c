//WAP to find the volume of a tromboloid using 4 functions.

#include<stdio.h>

float input_h()
{
       float x;
       printf("Enter the value of h of the tromboloid:\n");
       scanf("%f",&x);
       return x;
}
float input_b()
{
       float y;
       printf("Enter the value of b of the tromboloid:\n");
       scanf("%f",&y);
       return y;
}
float input_d()
{
       float z;
       printf("Enter the value of d of the tromoboloid:\n");
       scanf("%f",&z);
       return z;
}
float volume(float x,float y,float z)
{
       float a,b,vol;
       a=x*y*z;
       b=z/y;
       vol = 1.0/3.0 * (a+b);
       return vol;
}
float output(float vol)
{
      printf("The volume of the tromboloid is %f\n",vol);
}
int main()
{
      float h,b,d,v;
      h=input_h();
      b=input_b();
      d=input_d();
      v=volume(h,b,d);
      output(v);
      return 0;
}
