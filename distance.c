//WAP to find the distance between two point using 4 functions.

#include<stdio.h>
#include<math.h>

float input_1()
{
        float x1;
        printf("Enter the x coordinate:\n");
        scanf("%f",&x1);
}
float input_2()
{
       float y2;
       printf("Enter the y coordinates:\n");
       scanf("%f",&y2);
 }

float distance(float x1,float y1,float x2,float y2)
{
       
        float distance;
        distance = sqrt (((x2-x1)*(x2-x1)) +((y2-y1)*(y2-y1)));
        return distance;
}

float output(float distance)
{
        printf("The distance between the points is %f",distance);
}

int main()
{
       float a1,b1,a2,b2,dist;
       printf("Enter coordinates of first point and second point respectively\n");
       a1=input_1();
       b1=input_2();
       a2=input_1();
       b2=input_2();
       dist=distance(a1,b1,a2,b2);
       output(dist);
       return 0;
}