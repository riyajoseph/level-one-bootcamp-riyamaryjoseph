//WAP to find the sum of two fractions.

#include<stdio.h>

struct fraction
{
    int x,y;
};

typedef struct fraction fraction;

fraction input()
{
    fraction a;
    printf("Enter the numerator\n");
    scanf("%d", &a.x);
    printf("Enter the denominator\n");
    scanf("%d", &a.y);
    return a;
}

int gcd(int x, int y)
{
     int i,gcd;
     for(i=1; i<=x && i<=y; i++)
     {
	    if(x%i==0 && y%i==0)
           gcd=i;
     }
        return gcd;
}

fraction sum(fraction a, fraction b)
{
    int g;
    fraction final;
    final.x=((a.x*b.y) + (b.x*a.y));
    final.y=(a.y*b.y);
    g=gcd(final.x , final.y);
    final.x=((a.x*b.y) + (b.x*a.y))/g;
    final.y=(a.y*b.y)/g;
    return final;
}

fraction display(fraction a, fraction b, fraction final)
{
    printf("Sum of %d/%d and %d/%d = %d/%d", a.x, a.y, b.x, b.y, final.x, final.y);
}

int main()
{
    fraction a,b,s;
    a=input();
    b=input();
    s=sum(a,b);
    display(a,b,s);
    return 0;
}
