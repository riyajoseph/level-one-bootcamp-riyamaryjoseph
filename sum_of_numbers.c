//Write a program to find the sum of n different numbers using 4 functions

#include<stdio.h>
int input(int x,float *arr)
{ 

 	for(int i=0; i<x;i++)
	{
	  printf("Enter the number:");
	  scanf("%f",&arr[i]);
	}
}
int compute(int x,float arr[x])
{
	int sum=0;
	for(int i=0;i<x;i++)
	{
	 sum=sum+arr[i];
	}
return sum;
}
void display(int x,float sum)
{
	 printf("sum of %d numbers are:%f",x,sum);
}
int main()
{
float n,sum;
int x;
printf("Enter the value of n:");
scanf("%d",&x);
float arr[x];
input(x,&arr[0]);
sum=compute(x,arr);
display(x,sum);
return 0;
}

