//Write a program to find the volume of a tromboloid using one function

#include<stdio.h>
int main()
{ 
         float h,d,b,volume;
         printf("Enter the value of h\n");
         scanf("%f", &h);
         printf("Enter the value of d\n");
         scanf("%f", &d);
         printf("Enter the value of b\n");
         scanf("%f", &b);
         volume = (1.0 / 3.0) * ((h * b * d) + (d / b));
         printf("Volume of the tromboloid = %f",volume);
         return 0;
}    

